#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<pthread.h>

#define MAXLENGTH 12

void *ServerEcho(void *args)
{
  int clientFileDiscriptor=*((int*)(&args));
  char str[MAXLENGTH+1];
  while(1)        //loop infinity
  {
    read(clientFileDiscriptor,str,MAXLENGTH);
    //printf("reading from client:%s",str);
    write(clientFileDiscriptor,str,MAXLENGTH);
    //printf("echoing back to client");
  }
  close(clientFileDiscriptor);
}


int main()
{
  struct sockaddr_in sock_var;
  int serverFileDiscriptor=socket(AF_INET,SOCK_STREAM,0);
  int clientFileDiscriptor;
  int i;
  pthread_t* t = (pthread_t*)malloc(200*sizeof(pthread_t));

  sock_var.sin_addr.s_addr=inet_addr("127.0.0.1");
  sock_var.sin_port=3010;
  sock_var.sin_family=AF_INET;
  if(bind(serverFileDiscriptor,(struct sockaddr*)&sock_var,sizeof(sock_var))>=0)
  {
    printf("socket has been created \n");
    listen(serverFileDiscriptor,0);
    while(1)        //loop infinity
    {
      for(i=0;i<200;i++)      //can support 200 clients at a time
      {
        clientFileDiscriptor=accept(serverFileDiscriptor,NULL,NULL);
        printf("Connected to client %d \n",clientFileDiscriptor);
        pthread_create(t+i,NULL,ServerEcho,(void *)clientFileDiscriptor);
      }
    }
    close(serverFileDiscriptor);
  }
  else{
    printf("nsocket creation failed");
  }
  return 0;
}
