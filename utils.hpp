
#ifndef FLOAN_UTILS_HPP
#define FLOAN_UTILS_HPP

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include "openssl/sha.h"
#include <string.h>
#include <zconf.h>
#include <random>
#include "aes.hpp"

// Enable delay on the communication or not
#define USE_DELAY 1

// 0.13 ms
static const int INTERNAL_DELAY = 130000;
// 50 ms
static const int EXTERNAL_DELAY = 50000000;

void socket_startup();

void socket_cleanup();

void simulate_communication(const int delay);

uint8_t FFMul(uint8_t a, uint8_t b);

uint8_t exp254(uint8_t base);

void sha256(char *string, char outputBuffer[64]);

#endif //FLOAN_UTILS_HPP
