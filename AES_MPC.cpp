#include <iostream>
#include <thread>
#include "party.hpp"
#include "operationFactory.hpp"

using namespace std;

/*****************************************************************************/
/* Defines:                                                                  */
/*****************************************************************************/
// The number of columns comprising a state in AES. This is a constant in AES. Value=4
#define Nb 4
// The number of 32 bit words in a key.
#define Nk 4
// Key length in bytes [128 bit]
#define KEYLEN 16
// The number of rounds in AES Cipher.
#define Nr 10

typedef unsigned long state_t[4][4];

const uint8_t Rcon[255] = {
        0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
        0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
        0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
        0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
        0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
        0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
        0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,
        0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
        0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
        0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
        0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,
        0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
        0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
        0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,
        0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
        0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb  };


// This function produces Nb(Nr+1) round keys. The round keys are used in each round to decrypt the states.
void Party::KeyExpansion(const unsigned long* Key, unsigned long* RoundKey)
{
    uint32_t i, j;
    unsigned long k;
    unsigned long tempa[4]; // Used for the column/row operations

    // The first round key is the key itself.
    for(i = 0; i < Nk; ++i) {

        RoundKey[(i * 4) + 0] = 0;
        RoundKey[(i * 4) + 1] = 0;
        RoundKey[(i * 4) + 2] = 0;
        RoundKey[(i * 4) + 3] = 0;
        copyShare(Key[(i * 4) + 0], &RoundKey[(i * 4) + 0]);
        copyShare(Key[(i * 4) + 1], &RoundKey[(i * 4) + 1]);
        copyShare(Key[(i * 4) + 2], &RoundKey[(i * 4) + 2]);
        copyShare(Key[(i * 4) + 3], &RoundKey[(i * 4) + 3]);
    }

    // All other round keys are found from the previous round keys.
    for(; (i < (Nb * (Nr + 1))); ++i)
    {
        for(j = 0; j < 4; ++j)
        {
            tempa[j] = 0;
            copyShare(RoundKey[(i-1) * 4 + j], &tempa[j]);
        }
        if (i % Nk == 0)
        {
            // This function rotates the 4 bytes in a word to the left once.
            // [a0,a1,a2,a3] becomes [a1,a2,a3,a0]

            // Function RotWord()
            {
                k = tempa[0];
                tempa[0] = tempa[1];
                tempa[1] = tempa[2];
                tempa[2] = tempa[3];
                tempa[3] = k;
            }

            // SubWord() is a function that takes a four-byte input word and
            // applies the S-box to each of the four bytes to produce an output word.

            // Function Subword()
            {
                thread t1(&SBoxFactory,this, tempa[0], &tempa[0]);
                thread t2(&SBoxFactory,this, tempa[1], &tempa[1]);
                thread t3(&SBoxFactory,this, tempa[2], &tempa[2]);
                thread t4(&SBoxFactory,this, tempa[3], &tempa[3]);

                t1.join();
                t2.join();
                t3.join();
                t4.join();
            }

            XORcons(tempa[0], Rcon[i/Nk], &tempa[0]);
        }
        XOR(RoundKey[(i - Nk) * 4 + 0], tempa[0], &RoundKey[i * 4 + 0]);
        XOR(RoundKey[(i - Nk) * 4 + 1], tempa[1], &RoundKey[i * 4 + 1]);
        XOR(RoundKey[(i - Nk) * 4 + 2], tempa[2], &RoundKey[i * 4 + 2]);
        XOR(RoundKey[(i - Nk) * 4 + 3], tempa[3], &RoundKey[i * 4 + 3]);
    }
}

void AddRoundKey(Party* P, uint8_t round, state_t* state, const unsigned long* RoundKey)
{
    uint8_t i,j;
    for(i = 0; i < 4; ++i)
    {
        for(j = 0; j < 4; ++j)
        {
            P->XOR((*state)[i][j], RoundKey[round * Nb * 4 + i * Nb + j], &(*state)[i][j]);
        }
    }
}

void SubBytes(Party* P, state_t* state)
{

    thread t1(&SBoxFactory,P, (*state)[0][0], &(*state)[0][0]);
    thread t2(&SBoxFactory,P, (*state)[1][0], &(*state)[1][0]);
    thread t3(&SBoxFactory,P, (*state)[2][0], &(*state)[2][0]);
    thread t4(&SBoxFactory,P, (*state)[3][0], &(*state)[3][0]);
    thread t5(&SBoxFactory,P, (*state)[0][1], &(*state)[0][1]);
    thread t6(&SBoxFactory,P, (*state)[1][1], &(*state)[1][1]);
    thread t7(&SBoxFactory,P, (*state)[2][1], &(*state)[2][1]);
    thread t8(&SBoxFactory,P, (*state)[3][1], &(*state)[3][1]);
    thread t9(&SBoxFactory,P, (*state)[0][2], &(*state)[0][2]);
    thread t10(&SBoxFactory,P, (*state)[1][2], &(*state)[1][2]);
    thread t11(&SBoxFactory,P, (*state)[2][2], &(*state)[2][2]);
    thread t12(&SBoxFactory,P, (*state)[3][2], &(*state)[3][2]);
    thread t13(&SBoxFactory,P, (*state)[0][3], &(*state)[0][3]);
    thread t14(&SBoxFactory,P, (*state)[1][3], &(*state)[1][3]);
    thread t15(&SBoxFactory,P, (*state)[2][3], &(*state)[2][3]);
    thread t16(&SBoxFactory,P, (*state)[3][3], &(*state)[3][3]);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t6.join();
    t7.join();
    t8.join();
    t9.join();
    t10.join();
    t11.join();
    t12.join();
    t13.join();
    t14.join();
    t15.join();
    t16.join();

}

void ShiftRows(state_t* state)
{
    unsigned long temp;

    // Rotate first row 1 columns to left
    temp           = (*state)[0][1];
    (*state)[0][1] = (*state)[1][1];
    (*state)[1][1] = (*state)[2][1];
    (*state)[2][1] = (*state)[3][1];
    (*state)[3][1] = temp;

    // Rotate second row 2 columns to left
    temp           = (*state)[0][2];
    (*state)[0][2] = (*state)[2][2];
    (*state)[2][2] = temp;

    temp           = (*state)[1][2];
    (*state)[1][2] = (*state)[3][2];
    (*state)[3][2] = temp;

    // Rotate third row 3 columns to left
    temp           = (*state)[0][3];
    (*state)[0][3] = (*state)[3][3];
    (*state)[3][3] = (*state)[2][3];
    (*state)[2][3] = (*state)[1][3];
    (*state)[1][3] = temp;
}

unsigned long xtime(Party* P, unsigned long x)
{
    unsigned long checker = 0;
    P->part(x, 7, &checker);
    P->lengthen(checker, &checker);
    P->LS(x, 1, &x);
    P->ANDcons(checker, 27, &checker);
    P->XOR(x, checker, &x);
    return x;
}

void MixColumns(Party* P, state_t* state)
{
    uint8_t i;
    unsigned long Tmp = 0,
                  Tm  = 0,
                  t   = 0;
    for(i = 0; i < 4; ++i)
    {
        P->copyShare((*state)[i][0], &t);

        P->XOR((*state)[i][0], (*state)[i][1], &Tmp);
        P->XOR(Tmp, (*state)[i][2], &Tmp);
        P->XOR(Tmp, (*state)[i][3], &Tmp);

        P->XOR((*state)[i][0], (*state)[i][1], &Tm);
        Tm = xtime(P, Tm);
        P->XOR(Tm, Tmp, &Tm); P->XOR(Tm, (*state)[i][0], &(*state)[i][0]);

        P->XOR((*state)[i][1], (*state)[i][2], &Tm);
        Tm = xtime(P, Tm);
        P->XOR(Tm, Tmp, &Tm); P->XOR(Tm, (*state)[i][1], &(*state)[i][1]);

        P->XOR((*state)[i][2], (*state)[i][3], &Tm);
        Tm = xtime(P, Tm);
        P->XOR(Tm, Tmp, &Tm); P->XOR(Tm, (*state)[i][2], &(*state)[i][2]);

        P->XOR((*state)[i][3], t, &Tm);
        Tm = xtime(P, Tm);
        P->XOR(Tm, Tmp, &Tm); P->XOR(Tm, (*state)[i][3], &(*state)[i][3]);
    }
}

void Cipher(Party* P, state_t* state, const unsigned long* RoundKey)
{
    uint8_t round = 0;

    // Add the First round key to the state before starting the rounds.
    AddRoundKey(P, 0, state, RoundKey);

    // There will be Nr rounds.
    // The first Nr-1 rounds are identical.
    // These Nr-1 rounds are executed in the loop below.
    for(round = 1; round < Nr; ++round)
    {
        SubBytes(P, state); // The only cost of the operation is here
        ShiftRows(state);
        MixColumns(P, state);
        AddRoundKey(P, round, state, RoundKey);
    }

    // The last round is given below.
    // The MixColumns function is not here in the last round.
    SubBytes(P, state);
    ShiftRows(state);
    AddRoundKey(P, Nr, state, RoundKey);
}

void BlockCopy(unsigned long* output, unsigned long* input)
{
    uint8_t i;
    for (i=0;i<KEYLEN;++i)
    {
        output[i] = input[i];
    }
}

void Party::AES_MPC_encrypt(unsigned long *input, const unsigned long *key, unsigned long *output)
{
    // Copy input to output, and work in-memory on output
    BlockCopy(output, input);

    unsigned long* RoundKey = (unsigned long* )malloc(176*sizeof(unsigned long));
    for (int i = 0; i < 176; i++) {
        RoundKey[i] = 0;
    }
    state_t* state;
    state = (state_t*)output;

    // The Key input to the AES Program
    const unsigned long* Key;
    Key = key;
    KeyExpansion(Key, RoundKey);

    // The next function call encrypts the PlainText with the Key using AES algorithm.
    Cipher(this, state, RoundKey);

    free(RoundKey);
}

void XorWithIv(Party* P, unsigned long* buf, unsigned long* Iv)
{
    uint8_t i;
    for(i = 0; i < KEYLEN; ++i)
    {
        P->XOR(buf[i], Iv[i], &buf[i]);
    }
}

void Party::AES_MPC_CBC_encrypt(unsigned long* output, unsigned long* input, uint32_t length, const unsigned long* key, unsigned long* iv)
{
    uintptr_t i;

    BlockCopy(output, input);
    state_t* state;

    unsigned long* RoundKey = (unsigned long* )malloc(176*sizeof(unsigned long));
    for (i = 0; i < 176; i++) {
        RoundKey[i] = 0;
    }

    // The Key input to the AES Program
    const unsigned long* Key;
    Key = key;
    KeyExpansion(Key, RoundKey);

    unsigned long* Iv;
    Iv = iv;

    for(i = 0; i < length; i += KEYLEN)
    {
        XorWithIv(this, input, Iv);
        BlockCopy(output, input);
        state = (state_t*)output;
        Cipher(this, state, RoundKey);
        Iv = output;
        input = input + KEYLEN;
        output = output + KEYLEN;
    }

    free(RoundKey);

}

void Party::AES_MPC_CTR(unsigned long* input, const unsigned long* RoundKey, unsigned long counter, unsigned long* output)
{
    // Make a copy of the counter
    unsigned long counter_array[KEYLEN];
    for (int i = 0; i < 4; i++) {
        counter_array[i] = addShare((unsigned char)((counter>>8*i)&0xFF));
    }
    for (int i = 4; i < KEYLEN; i++) {
        counter_array[i] = addShare(0);
    }

    // Copy input to output, and work in-memory on output
    BlockCopy(output, counter_array);
    state_t* state;
    state = (state_t*)output;

    // The next function call encrypts the PlainText with the K using AES algorithm.
    Cipher(this, state, RoundKey);

    // XOR the input with the encrypted output of the counter
    for(int i=0;i<4;++i)
    {
        for(int j = 0; j < 4; ++j)
        {
            XOR((*state)[i][j], input[4*i+j], &(*state)[i][j]);
        }
    }
}

