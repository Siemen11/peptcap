
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <stdint.h>
#include <vector>
#include <unistd.h>
#include <unordered_map>
#include <bitset>
#include "aes.hpp"
#include "utils.hpp"
#include <boost/dynamic_bitset.hpp>

using namespace std;

using namespace boost;

#ifndef PARTY_HPP_
#define PARTY_HPP_

class Party {
private:
    vector<unsigned char> x, a;

    unsigned int big_counter  = 0,
                 counter      = 0,
                 k1, k2;

    unsigned char other_random[16];
    unsigned char random[16];
public:

    Party* first_party;
    Party* second_party;

    Party();

    void sync(Party* party1, Party* party2);

    void setk2(unsigned int k);

    unsigned long addShare(unsigned char value);

    unsigned long addShare(unsigned char x_temp, unsigned char a_temp);

    void clearSharelocal(unsigned long index);

    void clearShare(unsigned long index);

    unsigned long copySharelocal(unsigned long index, unsigned long* index3);

    void copyShare(unsigned long index, unsigned long* index3);

    void refillRandom();

    unsigned char getRandom();

    unsigned long shareRandlocal(unsigned long index3);

    void shareRand(unsigned long* index3);

    void setShare(unsigned long index, unsigned char first, unsigned char second);

    unsigned char getSharex(unsigned long index);

    unsigned char getSharea(unsigned long index);

    unsigned long partlocal(unsigned long index1, unsigned char i, unsigned long* index3);

    void part(unsigned long index1, unsigned char i, unsigned long* index3);

    unsigned long lengthenlocal(unsigned long index1, unsigned long* index3);

    void lengthen(unsigned long index1, unsigned long* index3);

    unsigned long XORlocal(unsigned long index1, unsigned long index2, unsigned long* index3);

    void XOR(unsigned long index1, unsigned long index2, unsigned long* index3);

    unsigned long XORlocalcons(unsigned long index1, unsigned char cons, unsigned long* index3);

    void XORcons(unsigned long index1, unsigned char cons, unsigned long* index3);

    unsigned long getInterAND(unsigned long index1, unsigned long index2, unsigned long index3);

    void ANDlocal(unsigned long index3);

    void AND(unsigned long index1, unsigned long index2, unsigned long *index3);

    unsigned long ANDlocalcons(unsigned long index1, unsigned char cons, unsigned long* index3);

    void ANDcons(unsigned long index1, unsigned char cons, unsigned long* index3);

    unsigned long NOTlocal(unsigned long index, unsigned long* index3);

    void NOT(unsigned long index, unsigned long* index3);

    void getInterEq(unsigned long index1, unsigned char offset, unsigned long index3);

    void Eqlocal(unsigned long index3);

    void Eq(unsigned long index1, unsigned long index2, unsigned long* index3);

    void Eqcons(unsigned long index1, unsigned char cons, unsigned long* index3);

    unsigned long LSlocal(unsigned long index, unsigned char shift, unsigned long* index3);

    void LS(unsigned long index, unsigned char shift, unsigned long* index3);

    unsigned long RSlocal(unsigned long index, unsigned char shift, unsigned long* index3);

    void RS(unsigned long index, unsigned char shift, unsigned long* index3);

    void FFMult(unsigned long index1, unsigned long index2, unsigned long *index3);

    void FFMultcons(unsigned long index1, unsigned char cons, unsigned long *index3);

    void power254(unsigned long index, unsigned long* index3);

    void vectorMult(unsigned long index, unsigned char* vector, unsigned long* index3);

    void KeyExpansion(const unsigned long* Key, unsigned long* RoundKey);

    void AES_MPC_encrypt(unsigned long *input, const unsigned long *key, unsigned long *output);

    void AES_MPC_CBC_encrypt(unsigned long* output, unsigned long* input, uint32_t length, const unsigned long* key, unsigned long* iv);

    void AES_MPC_CTR(unsigned long* input, const unsigned long* RoundKey, unsigned long counter, unsigned long* output);

    unsigned char open(unsigned long index);
};

#endif
