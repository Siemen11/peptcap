#include "protocol_tools.hpp"

// Receive an array of characters. We simulate the communication cost for this.
void receiveValue(unsigned char* out, unsigned char* in, unsigned int length) {

    // We assume that the other party is sending the shares via a non high speed connection.
    simulate_communication(EXTERNAL_DELAY);

    for (unsigned int i = 0; i < length; i++) {
        out[i] = in[i];
    }
}

// Make shares out of the value and add these to the parties. We simulate communication for this where we assume
// the sending party is using a low speed connection.
unsigned long sendNewShares(Party* P1, Party* P2, Party* P3, unsigned char value) {
    simulate_communication(EXTERNAL_DELAY);

    unsigned char r[3];

    r[0] = (unsigned char) (rand() & 0xFF);
    r[1] = (unsigned char) (rand() & 0xFF);
    r[2] = r[0]^r[1];

    P1->addShare(r[0], r[2]^value);
    P2->addShare(r[1], r[0]^value);
    return P3->addShare(r[2], r[1]^value);
}

// Make two character arrays containing the x and a shares of the value.
void makeShareBytes(int length, unsigned char* value, unsigned char** resultx, unsigned char** resulta) {
    unsigned char r[3];

    for (int j = 0; j < length; j++) {

        r[0] = (unsigned char) (rand() & 0xFF);
        r[1] = (unsigned char) (rand() & 0xFF);
        r[2] = r[0]^r[1];

        resultx[0][j] = r[0];
        resulta[0][j] = r[2]^value[j];
        resultx[1][j] = r[1];
        resulta[1][j] = r[0]^value[j];
        resultx[2][j] = r[2];
        resulta[2][j] = r[1]^value[j];
    }
}