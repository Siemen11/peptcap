#include <chrono>
#include <thread>
#include <rand.h>
#include <aes.h>
#include "car.hpp"

using namespace std;

// Length of the padded message for RSA for a symmetric key
#define RSALENK 256

// Length of the RSA keys for the booking details
#define RSALENM 2048

// Size of the key, the identities, etc... in bits
#define KEYSIZE   128
#define IDCARSIZE 32
#define CERTSIZE  7840
#define CDUCSIZE  96
#define ACUCSIZE  8
#define LCAR1SIZE 32
#define LCAR2SIZE 32
#define IDBSIZE   32
#define SIGNSIZE  512
#define PADSIZE   64
#define CBSIZE    128

// Booking format in bytes
#define LBOOKING 160
#define LPAD     6
#define LCCAR    176
#define LCB      96
#define ICERT    0
#define IIDCAR   64
#define ILCAR    68
#define ICDUC    76
#define IACUC    88
#define IIDB     89
#define ISIGMA   93

// number of bookings
static int NBOOKING;

// The IV for CBC mode encryption
static unsigned long IV[16];

// The counter for CTR mode encryption

static unsigned long counter;

// Initialise a consumer
void INI_consumer(Consumer* C, string ID) {
    C->ID = ID;

    unsigned char* Kc = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
    for (int i = 0; i < KEYSIZE/8; i++) {
        Kc[i] = (unsigned char)(rand()&0xFF);
    }

    C->Kc = Kc;

    C->counter = 0;

    // Generate public and private keys for the consumer
    generate_key(C->ID, RSALENM);
}

// Initialise an owner
void INI_owner(Owner* O, string ID) {
    O->ID = ID;

    // Generate public and private keys for the owner
    generate_key(O->ID, RSALENM);

}

// Initialise all the servers and the car manufacturer and create the database
void INI_servers(Server* CM, Server* S1, Server* S2, Server* S3, PublicLedger* PL, Database* db) {
    NBOOKING = 0;

    // Create ID's for the servers and initialise then to each other
    S1->ID = "S1";
    S2->ID = "S2";
    S3->ID = "S3";
    PL->ID = "PL";

    S1->S = new Party();
    S2->S = new Party();
    S3->S = new Party();
    CM->S = new Party();

    S1->S->sync(S2->S, S3->S);
    S2->S->sync(S3->S, S1->S);
    S3->S->sync(S1->S, S2->S);

    // Generate public and private keys for each server
    generate_key(S1->ID, RSALENM);
    generate_key(S2->ID, RSALENM);
    generate_key(S3->ID, RSALENM);
    generate_key(PL->ID, RSALENM);

    // Create names for the tables in the database (in reality these are separate databases)
    PL->ledger = "PLdb";

    CM->keyTable = "CMkeys";
    S1->keyTable = "S1keys";
    S2->keyTable = "S2keys";
    S3->keyTable = "S3keys";

    S1->bookingTable = "S1booking";
    S2->bookingTable = "S2booking";
    S3->bookingTable = "S3booking";

    // Create the actual tables
    string query_string;

    query_string = "CREATE TABLE IF NOT EXISTS "+PL->ledger+" (Time INTEGER PRIMARY KEY, Cuccar1x TEXT, Cuccar1a TEXT, Cuccar2x TEXT, Cuccar2a TEXT, Cuccar3x TEXT, Cuccar3a TEXT, Cb1x TEXT, Cb1a TEXT, Cb2x TEXT, Cb2a TEXT, Cb3x TEXT, Cb3a TEXT);";
    db->query(query_string.c_str());

    query_string = "CREATE TABLE IF NOT EXISTS "+CM->keyTable+" (IDo INTEGER, IDcar TEXT UNIQUE, key TEXT);";
    db->query(query_string.c_str());

    query_string = "CREATE TABLE IF NOT EXISTS "+S1->keyTable+" (IDo INTEGER, IDcar INTEGER UNIQUE, key INTEGER);";
    db->query(query_string.c_str());

    query_string = "CREATE TABLE IF NOT EXISTS "+S2->keyTable+" (IDo INTEGER, IDcar INTEGER UNIQUE, key INTEGER);";
    db->query(query_string.c_str());

    query_string = "CREATE TABLE IF NOT EXISTS "+S3->keyTable+" (IDo INTEGER, IDcar INTEGER UNIQUE, key INTEGER);";
    db->query(query_string.c_str());

    query_string = "CREATE TABLE IF NOT EXISTS "+S1->bookingTable+" (IDbserver INTEGER PRIMARY KEY AUTOINCREMENT, Cert INTEGER, IDcar INTEGER, Lcar INTEGER, CDuc INTEGER, ACuc INTEGER, sigma INTEGER, IDb INTEGER);";
    db->query(query_string.c_str());

    query_string = "CREATE TABLE IF NOT EXISTS "+S2->bookingTable+" (IDbserver INTEGER PRIMARY KEY AUTOINCREMENT, Cert INTEGER, IDcar INTEGER, Lcar INTEGER, CDuc INTEGER, ACuc INTEGER, sigma INTEGER, IDb INTEGER);";
    db->query(query_string.c_str());

    query_string = "CREATE TABLE IF NOT EXISTS "+S3->bookingTable+" (IDbserver INTEGER PRIMARY KEY AUTOINCREMENT, Cert INTEGER, IDcar INTEGER, Lcar INTEGER, CDuc INTEGER, ACuc INTEGER, sigma INTEGER, IDb INTEGER);";
    db->query(query_string.c_str());

    // Initialise the IV for CBC encryption to zero (In reality this is unsafe and needs to be initialized randomly and communicated to the other party)
    for (int i = 0; i < 16; i++) {
        IV[i] = S1->S->addShare(0);
    }

    // Initialise the counter for CTR encryption to zero
    counter = 0;
}

// Add a new car to the database, this involves adding a new car identity and its key
// IDo   is of size 32,
// IDcar is of size 32,
// key   is of size 128
void addKey(Server* CM, Server* S1, Server* S2, Server* S3, Database* db, string IDo, string IDcar, string key) {
    unsigned long ID_index    = 0,
                  key_index   = 0;
    int IDcar_value = atoi(IDcar.c_str());

    unsigned char key_value;

    // The CM keeps the car identity and key in open form in its database
    string query_CM = "INSERT OR IGNORE INTO "+CM->keyTable+" (IDo, IDcar,key) VALUES( '"+IDo+"', '"+IDcar+"', '"+key+"');";
    db->query(query_CM.c_str());

    // The CM then makes the car identity and key into shares and sends them to the three servers
    // These are encrypted via TLS
    for (int i = 0; i < IDCARSIZE/8; i++) {
        ID_index = sendNewShares(S1->S, S2->S, S3->S, (unsigned char)((IDcar_value>>8*i)&0xFF));
    }
    ID_index = ID_index - IDCARSIZE/8 + 1;

    for (int j = KEYSIZE/8-1; j >= 0; j--) {
        key_value = (unsigned char) strtol(key.substr((unsigned int) j*2, 2).c_str(), NULL, 16);

        key_index = sendNewShares(S1->S, S2->S, S3->S, key_value);
    }
    key_index = key_index - KEYSIZE/8 + 1;

    // The three servers then add these shares to their database
    string query_S1 = "INSERT OR IGNORE INTO "+S1->keyTable+"(IDo, IDcar,key) VALUES("+IDo+", "+to_string(ID_index)+", "+to_string(key_index)+");";
    db->query(query_S1.c_str());

    string query_S2 = "INSERT OR IGNORE INTO "+S2->keyTable+"(IDo, IDcar,key) VALUES("+IDo+", "+to_string(ID_index)+", "+to_string(key_index)+");";
    db->query(query_S2.c_str());

    string query_S3 = "INSERT OR IGNORE INTO "+S3->keyTable+"(IDo, IDcar,key) VALUES("+IDo+", "+to_string(ID_index)+", "+to_string(key_index)+");";
    db->query(query_S3.c_str());

}

// The booking details are given in a plain text file in reality these are agreed between the consumer and the owner
// This function parses in the details given in "booking_details"
bool readBookingDetails(unsigned char* M, const char* booking_file) {
    string line,
           certificate = "",
           signature   = "";

    unsigned char CDuc[CDUCSIZE],
                  ACuc = ' ';

    char* cert_array = (char*)malloc((CERTSIZE/8+1)*sizeof(char));
    memset(cert_array, 0, (CERTSIZE/8+1)*sizeof(char));
    char hashed_cert[64];

    int IDcar    = 0,
        IDb      = 0,
        M_length = 0;

    float Lcar1 = 0,
          Lcar2 = 0;

    // Open the file containing the booking details
    ifstream fin(booking_file, ifstream::in);
    if (!fin.good()) {
        fprintf(stderr, "Error: could not open the booking file %s\n", booking_file);
        return false;
    }

    // Read in the file ------------------------------------------------------------

    while (getline(fin, line)) {
        if(line == "-----BEGIN CERTIFICATE-----") {
            break;
        }
    }

    while (getline(fin, line)) {
        if (line == "-----END CERTIFICATE-----") {
            break;
        }

        certificate = certificate+line;
    }

    for(int i = 0; i < CERTSIZE/8+1; i++) {
        cert_array[i] = (unsigned char)certificate[i];
    }

    // We hash the certificate
    sha256(cert_array, hashed_cert);

    while (getline(fin, line)){
        if(line[0] == '#') {
            continue;
        }

        if (line.find("IDcar") != string::npos) {
            IDcar = stoi(line.substr(line.find("=") + 1).c_str(), 0, IDCARSIZE);
        }

        // We read these in as floats of 32 bits
        if (line.find("Lcar") != string::npos) {
            line = line.substr(line.find("=") + 1);
            Lcar1 = stof(line.substr(0, line.find(",")-1).c_str(), 0);
            Lcar2 = stof(line.substr(line.find(",") + 2).c_str(), 0);
        }

        if (line.find("CDuc") != string::npos) {
            line = line.substr(line.find("=") + 1);
            for (int i = 0; i < CDUCSIZE; i++) {
                CDuc[i] = (unsigned char)line[i];
            }
        }

        if (line.find("ACuc") != string::npos) {
            line = line.substr(line.find("=") + 1);
            ACuc = (unsigned char)line[0];
        }

        if (line.find("IDb") != string::npos) {
            IDb = stoi(line.substr(line.find("=") + 1).c_str(), 0, IDBSIZE);
        }

        if(line == "-----END BOOKING DETAILS-----") {
            break;
        }
    }

    // Create M --------------------------------------------------------------------

    for(int i = 0; i < SIGNSIZE/8; i++) {
        M[M_length+i] = (unsigned char)hashed_cert[i];
    }
    M_length += SIGNSIZE/8;

    for (int i = 0; i < IDCARSIZE/8; i++) {
        M[M_length+i] = (unsigned char)((IDcar>>(8*i))&0xFF);
    }
    M_length += IDCARSIZE/8;

    unsigned char* location1 = (unsigned char *) &Lcar1;
    unsigned char* location2 = (unsigned char *) &Lcar2;

    for (int i = 0; i < LCAR1SIZE/8; i++) {
        M[M_length+i] = location1[i];
    }
    M_length += LCAR1SIZE/8;

    for (int i = 0; i < LCAR2SIZE/8; i++) {
        M[M_length+i] = location2[i];
    }
    M_length += LCAR2SIZE/8;

    for (int i = 0; i < CDUCSIZE/8; i++) {
        M[M_length+i] = CDuc[i];
    }
    M_length += CDUCSIZE/8;

    M[M_length] = ACuc;
    M_length += ACUCSIZE/8;

    for (int i = 0; i < IDBSIZE/8; i++) {
        M[M_length+i] = (unsigned char)((IDb>>(8*i))&0xFF);
    }
    M_length += IDBSIZE/8;

    for(int i = 0; i < SIGNSIZE/8 + PADSIZE/8; i++) {
        M[M_length+i] = 0;
    }

    free(cert_array);

    // Optional print out of M------------------------------------------------------
    // for(int i = 0; i < M_length; i++) {
    //   cout << (int)M[i];
    // }
    // cout << endl;
    // cout << M_length << "bytes or " << M_length*8 << "bits" << endl;

    return true;
}

// The consumer sends his generated K1, K2 to the servers via public key encryption
void sendK1K2(Consumer* C, Owner* O, Server* S1, Server* S2, Server* S3, unsigned long* K1_index, unsigned long* K2_index) {
    unsigned char* K1;
    unsigned char* K2;
    unsigned char* C1;
    unsigned char* C2;
    unsigned char* Kx[3];
    unsigned char* Ka[3];
    unsigned char* K2x[3];
    unsigned char* K2a[3];

    unsigned char* encKx[3];
    unsigned char* encKa[3];
    unsigned char* encKxReceived[3];
    unsigned char* encKaReceived[3];
    unsigned char* decrKx[3];
    unsigned char* decrKa[3];

    unsigned long tempK1index = 0,
                  tempK2index = 0;

    Server* servers[3] = {S1, S2, S3};

    K1 = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
    C1 = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
    K2 = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
    C2 = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);

    for (int i = 0; i < 3; i++){
        K2x[i] = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
        K2a[i] = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
        Kx[i] = (unsigned char*)malloc(sizeof(unsigned char)*2*KEYSIZE/8);
        Ka[i] = (unsigned char*)malloc(sizeof(unsigned char)*2*KEYSIZE/8);
        encKx[i] = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
        encKa[i] = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
        encKxReceived[i] = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
        encKaReceived[i] = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
        decrKx[i] = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
        decrKa[i] = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
    }

    // Generate K1 and K2
    AES_KEY enc_key;
    AES_set_encrypt_key(C->Kc, KEYSIZE, &enc_key);
    for (int i = 0; i < 16; i++) {
        C1[i] = (unsigned char)((C->counter>>8*i)&0xFF);
        C2[i] = (unsigned char)(((C->counter+1)>>8*i)&0xFF);
    }

    unsigned char iv_enc[AES_BLOCK_SIZE];
    memset(iv_enc, 0, sizeof(iv_enc));

    AES_cbc_encrypt(C1, K1, KEYSIZE/8, &enc_key, iv_enc, AES_ENCRYPT);
    AES_cbc_encrypt(C2, K2, KEYSIZE/8, &enc_key, iv_enc, AES_ENCRYPT);

    C->counter += 2;

    // The consumer storages this key to decrypt the access oken later
    C->K1 = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
    C->K2 = (unsigned char*)malloc(sizeof(unsigned char)*KEYSIZE/8);
    for (int i = 0; i < 16; i++) {
        C->K1[i] = K1[i];
        C->K2[i] = K2[i];
    }

    // Make the shares
    makeShareBytes(KEYSIZE/8, K1, Kx, Ka);

    makeShareBytes(KEYSIZE/8, K2, K2x, K2a);
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < KEYSIZE/8; j++) {
            Kx[i][j+KEYSIZE/8] = K2x[i][j];
            Ka[i][j+KEYSIZE/8] = K2a[i][j];
        }
    }

    // Encrypt the newly made shares
    thread t1(&public_encrypt, Kx[0], 2*KEYSIZE/8, servers[0]->ID, encKx[0]);
    thread t2(&public_encrypt, Ka[0], 2*KEYSIZE/8, servers[0]->ID, encKa[0]);
    thread t3(&public_encrypt, Kx[1], 2*KEYSIZE/8, servers[1]->ID, encKx[1]);
    thread t4(&public_encrypt, Ka[1], 2*KEYSIZE/8, servers[1]->ID, encKa[1]);
    thread t5(&public_encrypt, Kx[2], 2*KEYSIZE/8, servers[2]->ID, encKx[2]);
    thread t6(&public_encrypt, Ka[2], 2*KEYSIZE/8, servers[2]->ID, encKa[2]);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t6.join();

    // Send to owner
    thread t1n(&receiveValue, encKx[0], encKx[0], RSALENK);
    thread t2n(&receiveValue, encKa[0], encKa[0], RSALENK);
    thread t3n(&receiveValue, encKx[1], encKx[1], RSALENK);
    thread t4n(&receiveValue, encKa[1], encKa[1], RSALENK);
    thread t5n(&receiveValue, encKx[2], encKx[2], RSALENK);
    thread t6n(&receiveValue, encKa[2], encKa[2], RSALENK);

    t1n.join();
    t2n.join();
    t3n.join();
    t4n.join();
    t5n.join();
    t6n.join();

    // Simulate the TLS handshake to make a symmetric session key, this is just a simulation to match time consumption.
    unsigned char* temp_in = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK/8);
    unsigned char* temp_out = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
    RAND_bytes(temp_in, RSALENK/8);

    thread t1o(&private_encrypt, temp_in, RSALENK/8, O->ID, temp_out);
    thread t2o(&private_encrypt, temp_out, RSALENK/8, servers[0]->ID, temp_out);
    thread t3o(&private_encrypt, temp_in, RSALENK/8, O->ID, temp_out);
    thread t4o(&private_encrypt, temp_out, RSALENK/8, servers[1]->ID, temp_out);
    thread t5o(&private_encrypt, temp_in, RSALENK/8, O->ID, temp_out);
    thread t6o(&private_encrypt, temp_out, RSALENK/8, servers[2]->ID, temp_out);

    t1o.join();
    t2o.join();
    t3o.join();
    t4o.join();
    t5o.join();
    t6o.join();

    // Send to servers
    thread t1v(&receiveValue, encKxReceived[0], encKx[0], RSALENK);
    thread t2v(&receiveValue, encKaReceived[0], encKa[0], RSALENK);
    thread t3v(&receiveValue, encKxReceived[1], encKx[1], RSALENK);
    thread t4v(&receiveValue, encKaReceived[1], encKa[1], RSALENK);
    thread t5v(&receiveValue, encKxReceived[2], encKx[2], RSALENK);
    thread t6v(&receiveValue, encKaReceived[2], encKa[2], RSALENK);

    t1v.join();
    t2v.join();
    t3v.join();
    t4v.join();
    t5v.join();
    t6v.join();

    // The servers decrypt with their secret key the values they just received
    thread t1m(&private_decrypt, encKxReceived[0], RSALENK, servers[0]->ID, decrKx[0]);
    thread t2m(&private_decrypt, encKaReceived[0], RSALENK, servers[0]->ID, decrKa[0]);
    thread t3m(&private_decrypt, encKxReceived[1], RSALENK, servers[1]->ID, decrKx[1]);
    thread t4m(&private_decrypt, encKaReceived[1], RSALENK, servers[1]->ID, decrKa[1]);
    thread t5m(&private_decrypt, encKxReceived[2], RSALENK, servers[2]->ID, decrKx[2]);
    thread t6m(&private_decrypt, encKaReceived[2], RSALENK, servers[2]->ID, decrKa[2]);

    t1m.join();
    t2m.join();
    t3m.join();
    t4m.join();
    t5m.join();
    t6m.join();

    // The servers then add these shares
    for (int j = 0; j < KEYSIZE/8; j++) {
        tempK1index = S1->S->addShare(decrKx[0][j], decrKa[0][j]);
    }

    for (int j = 0; j < KEYSIZE/8; j++) {
        tempK1index = S2->S->addShare(decrKx[1][j], decrKa[1][j]);
    }

    for (int j = 0; j < KEYSIZE/8; j++) {
        tempK1index = S3->S->addShare(decrKx[2][j], decrKa[2][j]);
    }

    *K1_index = tempK1index - KEYSIZE/8 + 1;

    for (int j = 0; j < KEYSIZE/8; j++) {
        tempK2index = S1->S->addShare(decrKx[0][j+KEYSIZE/8], decrKa[0][j+KEYSIZE/8]);
    }

    for (int j = 0; j < KEYSIZE/8; j++) {
        tempK2index = S2->S->addShare(decrKx[1][j+KEYSIZE/8], decrKa[1][j+KEYSIZE/8]);
    }

    for (int j = 0; j < KEYSIZE/8; j++) {
        tempK2index = S3->S->addShare(decrKx[2][j+KEYSIZE/8], decrKa[2][j+KEYSIZE/8]);
    }

    *K2_index = tempK2index - KEYSIZE/8 + 1;

    for (int i = 0; i < 3; i++) {
        free(Kx[i]);
        free(Ka[i]);
        free(K2x[i]);
        free(K2a[i]);
        free(encKx[i]);
        free(encKa[i]);
        free(encKxReceived[i]);
        free(encKaReceived[i]);
        free(decrKx[i]);
        free(decrKa[i]);
    }

    free(K1);
    free(K2);
    free(C1);
    free(C2);
    free(temp_in);
    free(temp_out);

}

// The booking details are then shared to the servers
void shareM(Owner* owner, Server* S1, Server* S2, Server* S3, Database* db, unsigned char* M, int* booking_server_id, unsigned long* booking_index) {
    unsigned long Ibooking = 0;
    unsigned char* Mx[3];
    unsigned char* Ma[3];

    int packets = LBOOKING/(RSALENK/8)+1;

    // Before we share M the owner needs to sign the booking details
    unsigned char Mb[ISIGMA];

    unsigned char* MxReceived[3];
    unsigned char* MaReceived[3];

    char* sign = (char*)malloc(RSALENM*sizeof(char));
    memset(sign, 0, RSALENM*sizeof(char));

    char* hashed_sign = (char*)malloc(SIGNSIZE*sizeof(char));
    memset(hashed_sign, 0, SIGNSIZE*sizeof(char));

    for (int i = 0; i < ISIGMA; i++) {
        Mb[i] = M[i];
    }

    for (int i = 0; i < 3; i++){
        Mx[i] = (unsigned char*)malloc(sizeof(unsigned char)*LBOOKING);
        Ma[i] = (unsigned char*)malloc(sizeof(unsigned char)*LBOOKING);
        MxReceived[i] = (unsigned char*)malloc(sizeof(unsigned char)*packets*RSALENK);
        MaReceived[i] = (unsigned char*)malloc(sizeof(unsigned char)*packets*RSALENK);
    }

    // The owner signs the booking details
    private_encrypt(Mb, ISIGMA, owner->ID, (unsigned char*)sign);

    // This signature is then hashed to reduce its length (it becomes 512 bits long)
    sha256(sign, hashed_sign);

    for (int i = 0; i < SIGNSIZE/8; i++) {
        M[i+ISIGMA] = (unsigned char) hashed_sign[i];
    }

    // Make the shares
    makeShareBytes(LBOOKING, M, Mx, Ma);

    // Send symmetrically encrypted booking the servers (the TLS handshake was done previously)
    thread t1n(&receiveValue, MxReceived[0], Mx[0], LBOOKING);
    thread t2n(&receiveValue, MaReceived[0], Ma[0], LBOOKING);
    thread t3n(&receiveValue, MxReceived[1], Mx[1], LBOOKING);
    thread t4n(&receiveValue, MaReceived[1], Ma[1], LBOOKING);
    thread t5n(&receiveValue, MxReceived[2], Mx[2], LBOOKING);
    thread t6n(&receiveValue, MaReceived[2], Ma[2], LBOOKING);

    t1n.join();
    t2n.join();
    t3n.join();
    t4n.join();
    t5n.join();
    t6n.join();

    // The servers then add these shares
    for (int j = 0; j < LBOOKING; j++) {
        Ibooking = S1->S->addShare(MxReceived[0][j], MaReceived[0][j]);
    }

    for (int j = 0; j < LBOOKING; j++) {
        S2->S->addShare(MxReceived[1][j], MaReceived[1][j]);
    }

    for (int j = 0; j < LBOOKING; j++) {
        S3->S->addShare(MxReceived[2][j], MaReceived[2][j]);
    }

    *booking_index = Ibooking - LBOOKING + 1;

    // The servers then add these shares into their database such that if an incident occurs these servers can find the original booking details again
    string query_S1 = "INSERT OR IGNORE INTO "+S1->bookingTable+"(Cert, IDcar, Lcar, CDuc, ACuc, sigma, IDb) VALUES("+to_string(*booking_index+ICERT)+", "+to_string(*booking_index+IIDCAR)+", "+to_string(*booking_index+ILCAR)+", "+to_string(*booking_index+ICDUC)+", "+to_string(*booking_index+IACUC)+", "+to_string(*booking_index+ISIGMA)+", "+to_string(*booking_index+IIDB)+");";
    db->query(query_S1.c_str());

    string query_S2 = "INSERT OR IGNORE INTO "+S2->bookingTable+"(Cert, IDcar, Lcar, CDuc, ACuc, sigma, IDb) VALUES("+to_string(*booking_index+ICERT)+", "+to_string(*booking_index+IIDCAR)+", "+to_string(*booking_index+ILCAR)+", "+to_string(*booking_index+ICDUC)+", "+to_string(*booking_index+IACUC)+", "+to_string(*booking_index+ISIGMA)+", "+to_string(*booking_index+IIDB)+");";
    db->query(query_S2.c_str());

    string query_S3 = "INSERT OR IGNORE INTO "+S3->bookingTable+"(Cert, IDcar, Lcar, CDuc, ACuc, sigma, IDb) VALUES("+to_string(*booking_index+ICERT)+", "+to_string(*booking_index+IIDCAR)+", "+to_string(*booking_index+ILCAR)+", "+to_string(*booking_index+ICDUC)+", "+to_string(*booking_index+IACUC)+", "+to_string(*booking_index+ISIGMA)+", "+to_string(*booking_index+IIDB)+");";
    db->query(query_S3.c_str());

    NBOOKING++;
    *booking_server_id = NBOOKING;

    free(sign);
    free(hashed_sign);
    for (int S = 0; S < 3; S++) {
        free(Mx[S]);
        free(Ma[S]);
        free(MxReceived[S]);
        free(MaReceived[S]);
    }
}

// The owner then chooses the correct car key in an oblivious way
void extractKey(string identity_owner, Server* S1, Database* db, int booking_id, unsigned long* key_index) {

    // Select all car keys from the owner
    string query_K = "SELECT IDcar, key FROM "+S1->keyTable+" WHERE IDo='"+identity_owner+"';";
    vector<vector<string> > resultK = db->query(query_K.c_str());

    int l = (int)resultK.size();

    unsigned long equal = 0,
                  temp1 = 0,
                  temp2 = 0,
                  IDbookingcar;
    unsigned long result[l][16], IDcars[l], keys[l];
    for (int i = 0; i < l; i++) {
        for (int j = 0; j < 16; j++) {
            result[i][j] = S1->S->addShare(0);
        }
    }

    for(vector<vector<string> >::iterator it = resultK.begin(); it < resultK.end(); ++it)
    {
        vector<string> row = *it;
        IDcars[it - resultK.begin()] = stoul(row.at(0));
        keys[it - resultK.begin()] = stoul(row.at(1));
    }

    // Select the booking details that were shared
    string query_M = "SELECT IDcar FROM "+S1->bookingTable+" WHERE IDbserver='"+to_string(booking_id)+"';";
    vector<vector<string> > resultM = db->query(query_M.c_str());
    vector<vector<string> >::iterator itM = resultM.begin();
    vector<string> rowM = *itM;
    IDbookingcar = stoul(rowM.at(0));

    // Go through all the car keys of the owner and select the one specified in the booking details in an oblivious way
    for (int i = 0; i < l; i++) {
        S1->S->Eq(IDbookingcar, IDcars[i], &temp1);
        S1->S->Eq(IDbookingcar+1, IDcars[i]+1, &temp2);
        S1->S->AND(temp1, temp2, &equal);

        S1->S->Eq(IDbookingcar+2, IDcars[i]+2, &temp1);
        S1->S->Eq(IDbookingcar+3, IDcars[i]+3, &temp2);
        S1->S->AND(temp1, temp2, &temp1);

        S1->S->AND(temp1, equal, &equal);

        S1->S->lengthen(equal, &equal);

        // The ANDs can be done in parallel normally
        for (int j = 0; j < 16; j++) {
            S1->S->AND(equal, keys[i]+j, &result[i][j]);
        }
    }

    for (int i = 1; i < l; i++) {
        for (int j = 0; j < 16; j++) {
            S1->S->XOR(result[0][j], result[i][j], &result[0][j]);
        }
    }

    *key_index = result[0][0];
}

// Make the access token to the car
void makeToken(Server* S1, unsigned long booking_index, unsigned long key_index, unsigned long K1_index, unsigned long K2_index, unsigned long* Cuccar_index, unsigned long* Cb_index) {

    // Set up all the information that was given: keys, booking details
    unsigned long* Muc = (unsigned long*)malloc((LCCAR)*sizeof(unsigned long));
    for (int i = 0; i < LBOOKING; i++) {
        Muc[i] = booking_index+i;
    }

    unsigned long Mb[LCB];
    for (int i = 0; i < ISIGMA; i++) {
        Mb[i] = booking_index+i;
    }
    for (int i = 0; i < LCB-ISIGMA; i++) {
        Mb[i+ISIGMA] = S1->S->addShare(0);
    }

    for (int i = 0; i < 4; i++) {
        Muc[LBOOKING+i] = Mb[IIDCAR+i];
    }
    for (int i = 0; i < 12; i++) {
        Muc[LBOOKING+4+i] = S1->S->addShare(0);
    }

    unsigned long Kcar[16];
    for (int i = 0; i < 16; i++) {
        Kcar[15-i] = key_index+i;
    }

    unsigned long K1[16];
    for (int i = 0; i < 16; i++) {
        K1[i] = K1_index+i;
    }

    unsigned long K2[16];
    for (int i = 0; i < 16; i++) {
        K2[i] = K2_index+i;
    }

    unsigned long* Ccar = (unsigned long*)malloc(LCCAR*sizeof(unsigned long));
    memset(Ccar, 0, LCCAR*sizeof(unsigned long));

    // Encrypt a first time with the car key
    // We parallelise the AES calls

    unsigned long* RoundKey = (unsigned long*)malloc(176*sizeof(unsigned long));
    memset(RoundKey, 0, 176*sizeof(unsigned long));

    KeyExpansionFactory(S1->S, Kcar, RoundKey);

    thread t1 (&AESCTRFactory, S1->S, Muc            , RoundKey, counter  , Ccar            );
    thread t2 (&AESCTRFactory, S1->S, Muc+  KEYSIZE/8, RoundKey, counter+1, Ccar+  KEYSIZE/8);
    thread t3 (&AESCTRFactory, S1->S, Muc+2*KEYSIZE/8, RoundKey, counter+2, Ccar+2*KEYSIZE/8);
    thread t4 (&AESCTRFactory, S1->S, Muc+3*KEYSIZE/8, RoundKey, counter+3, Ccar+3*KEYSIZE/8);
    thread t5 (&AESCTRFactory, S1->S, Muc+4*KEYSIZE/8, RoundKey, counter+4, Ccar+4*KEYSIZE/8);
    thread t6 (&AESCTRFactory, S1->S, Muc+5*KEYSIZE/8, RoundKey, counter+5, Ccar+5*KEYSIZE/8);
    thread t7 (&AESCTRFactory, S1->S, Muc+6*KEYSIZE/8, RoundKey, counter+6, Ccar+6*KEYSIZE/8);
    thread t8 (&AESCTRFactory, S1->S, Muc+7*KEYSIZE/8, RoundKey, counter+7, Ccar+7*KEYSIZE/8);
    thread t9 (&AESCTRFactory, S1->S, Muc+8*KEYSIZE/8, RoundKey, counter+8, Ccar+8*KEYSIZE/8);
    thread t10(&AESCTRFactory, S1->S, Muc+9*KEYSIZE/8, RoundKey, counter+9, Ccar+9*KEYSIZE/8);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t6.join();
    t7.join();
    t8.join();
    t9.join();
    t10.join();

    counter += 10;

    // Concatenate IDcar to the previous encryption result
    for (int i = 0; i < 4; i++) {
        Ccar[LBOOKING+i] = Mb[IIDCAR+i];
    }
    for (int i = 0; i < 12; i++) {
        Ccar[LBOOKING+4+i] = S1->S->addShare(0);
    }

    unsigned long* Cuccar = (unsigned long*)malloc(LCCAR*sizeof(unsigned long));
    memset(Cuccar, 0, LCCAR*sizeof(unsigned long));

    // Encrypt a second time with the key K1 specified by the consumer
    // We parallelise the AES calls

    KeyExpansionFactory(S1->S, K1, RoundKey);

    thread t1n (&AESCTRFactory, S1->S, Ccar             , RoundKey, counter   , Cuccar             );
    thread t2n (&AESCTRFactory, S1->S, Ccar+   KEYSIZE/8, RoundKey, counter+1 , Cuccar+   KEYSIZE/8);
    thread t3n (&AESCTRFactory, S1->S, Ccar+ 2*KEYSIZE/8, RoundKey, counter+2 , Cuccar+ 2*KEYSIZE/8);
    thread t4n (&AESCTRFactory, S1->S, Ccar+ 3*KEYSIZE/8, RoundKey, counter+3 , Cuccar+ 3*KEYSIZE/8);
    thread t5n (&AESCTRFactory, S1->S, Ccar+ 4*KEYSIZE/8, RoundKey, counter+4 , Cuccar+ 4*KEYSIZE/8);
    thread t6n (&AESCTRFactory, S1->S, Ccar+ 5*KEYSIZE/8, RoundKey, counter+5 , Cuccar+ 5*KEYSIZE/8);
    thread t7n (&AESCTRFactory, S1->S, Ccar+ 6*KEYSIZE/8, RoundKey, counter+6 , Cuccar+ 6*KEYSIZE/8);
    thread t8n (&AESCTRFactory, S1->S, Ccar+ 7*KEYSIZE/8, RoundKey, counter+7 , Cuccar+ 7*KEYSIZE/8);
    thread t9n (&AESCTRFactory, S1->S, Ccar+ 8*KEYSIZE/8, RoundKey, counter+8 , Cuccar+ 8*KEYSIZE/8);
    thread t10n(&AESCTRFactory, S1->S, Ccar+ 9*KEYSIZE/8, RoundKey, counter+9 , Cuccar+ 9*KEYSIZE/8);
    thread t11n(&AESCTRFactory, S1->S, Ccar+10*KEYSIZE/8, RoundKey, counter+10, Cuccar+10*KEYSIZE/8);

    t1n.join();
    t2n.join();
    t3n.join();
    t4n.join();
    t5n.join();
    t6n.join();
    t7n.join();
    t8n.join();
    t9n.join();
    t10n.join();
    t11n.join();

    for (int i = 0; i < LCCAR; i++) {
        Cuccar_index[i] = Cuccar[i];
    }

    unsigned long Cb[LCB];

    // Set the IV to zero
    for (int i = 0; i < 16; i++) {
        S1->S->clearShare(IV[i]);
    }

    // Encrypt the booking details again with the key K2 specified by the consumer
    S1->S->AES_MPC_CBC_encrypt(Cb, Mb, LCB, K2, IV);

    // Update the IV
    for (int i = 0; i < 16; i++) {
        IV[i] = Cb[LCB-16+i];
    }

    // Select only the last block of the CBC result
    for (int i = 0; i < 16; i++) {
        Cb_index[i] = Cb[LCB - 16 + i];
    }

    free(RoundKey);
    free(Muc);
    free(Cuccar);
}

// Send the shares of the access token to the public ledger where these shares or publicly stored
void sendToPL(Server* S1, Server* S2, Server* S3, PublicLedger* PL, Database* db, unsigned long* Cuccar_index, unsigned long* Cb_index, long* time_stamp) {
    unsigned char Cuccarx[3][LCCAR], Cuccara[3][LCCAR],
                  receivedCuccarx[3][LCCAR], receivedCuccara[3][LCCAR],
                  Cbx[3][CBSIZE/8], Cba[3][CBSIZE/8],
                  receivedCbx[3][CBSIZE/8], receivedCba[3][CBSIZE/8];

    string PLCuccarx[3], PLCuccara[3],
           PLCbx[3], PLCba[3];

    Server* servers[3] = {S1, S2, S3};

    time_t  timev;

    stringstream ss;

    // take the shares from the servers
    for (int P = 0; P < 3; P++) {
        for (int i = 0; i < LCCAR; i++) {
            Cuccarx[P][i] = servers[P]->S->getSharex(Cuccar_index[i]);
            Cuccara[P][i] = servers[P]->S->getSharea(Cuccar_index[i]);
        }
    }

    for (int P = 0; P < 3; P++) {
        for (int i = 0; i < CBSIZE/8; i++) {
            Cbx[P][i] = servers[P]->S->getSharex(Cb_index[i]);
            Cba[P][i] = servers[P]->S->getSharea(Cb_index[i]);
        }
    }

    // Send the value to the PL
    thread t1(&receiveValue, receivedCuccarx[0], Cuccarx[0], LCCAR);
    thread t2(&receiveValue, receivedCuccara[0], Cuccara[0], LCCAR);
    thread t3(&receiveValue, receivedCbx[0], Cbx[0], CBSIZE/8);
    thread t4(&receiveValue, receivedCba[0], Cba[0], CBSIZE/8);
    thread t5(&receiveValue, receivedCuccarx[1], Cuccarx[1], LCCAR);
    thread t6(&receiveValue, receivedCuccara[1], Cuccara[1], LCCAR);
    thread t7(&receiveValue, receivedCbx[1], Cbx[1], CBSIZE/8);
    thread t8(&receiveValue, receivedCba[1], Cba[1], CBSIZE/8);
    thread t9(&receiveValue, receivedCuccarx[2], Cuccarx[2], LCCAR);
    thread t10(&receiveValue, receivedCuccara[2], Cuccara[2], LCCAR);
    thread t11(&receiveValue, receivedCbx[2], Cbx[2], CBSIZE/8);
    thread t12(&receiveValue, receivedCba[2], Cba[2], CBSIZE/8);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t6.join();
    t7.join();
    t8.join();
    t9.join();
    t10.join();
    t11.join();
    t12.join();

    // Convert the character array to a string of hex values
    for (int P = 0; P < 3; P++) {
        ss.str("");
        ss.clear();
        for (int i = 0; i < LCCAR; i++) {
            ss << setw(2) << hex << (int) receivedCuccarx[P][i];
        }
        PLCuccarx[P] = ss.str();

        ss.str("");
        ss.clear();
        for (int i = 0; i < LCCAR; i++) {
            ss << setw(2) << hex << (int) receivedCuccara[P][i];
        }
        PLCuccara[P] = ss.str();
    }

    for (int P = 0; P < 3; P++) {
        ss.str("");
        ss.clear();
        for (int i = 0; i < 16; i++) {
            ss << setw(2) << hex << (int) receivedCbx[P][i];
        }
        PLCbx[P] = ss.str();

        ss.str("");
        ss.clear();
        for (int i = 0; i < 16; i++) {
            ss << setw(2) << hex << (int) receivedCba[P][i];
        }
        PLCba[P] = ss.str();
    }

    // The booking details are then time stamped such that each booking detail has a unique identifier
    *time_stamp = time(&timev);

    // The public ledger puts the separate shares of the booking details in the public database
    string query_PL = "INSERT OR IGNORE INTO "+PL->ledger+"(Time, Cuccar1x, Cuccar1a, Cuccar2x, Cuccar2a, Cuccar3x, Cuccar3a, Cb1x, Cb1a, Cb2x, Cb2a, Cb3x, Cb3a) VALUES("+to_string(*time_stamp)+", '"+PLCuccarx[0]+"', '"+PLCuccara[0]+"', '"+PLCuccarx[1]+"', '"+PLCuccara[1]+"', '"+PLCuccarx[2]+"', '"+PLCuccara[2]+"', '"+PLCbx[0]+"', '"+PLCba[0]+"', '"+PLCbx[1]+"', '"+PLCba[1]+"', '"+PLCbx[2]+"', '"+PLCba[2]+"');";
    db->query(query_PL.c_str());
}

void verifyToken(Consumer* C, PublicLedger* PL, Database* db, unsigned char* M, long time_stamp) {

    // Get the shares of the access token out of the public ledger via the time stamp
    string query_PL = "SELECT Cb1a, Cb3x FROM "+PL->ledger+" WHERE Time='"+to_string(time_stamp)+"';";
    vector<vector<string> > resultPL = db->query(query_PL.c_str());
    vector<vector<string> >::iterator it = resultPL.begin();
    vector<string> row = *it;

    unsigned char* CbaPL = (unsigned char*)malloc(16*sizeof(unsigned char));
    unsigned char* CbxPL = (unsigned char*)malloc(16*sizeof(unsigned char));
    unsigned char* Cba = (unsigned char*)malloc(16*sizeof(unsigned char));
    unsigned char* Cbx = (unsigned char*)malloc(16*sizeof(unsigned char));


    for (unsigned int i = 0; i < 16; i++) {
        CbaPL[i] = (unsigned char) strtol(row.at(0).substr(i*2, 2).c_str(), NULL, 16);
        CbxPL[i] = (unsigned char) strtol(row.at(1).substr(i*2, 2).c_str(), NULL, 16);
    }

    // Simulate the TLS handshake to make a symmetric session key, this is just a simulation to match time consumption.
    unsigned char* temp_in = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK/8);
    unsigned char* temp_out = (unsigned char*)malloc(sizeof(unsigned char)*RSALENK);
    RAND_bytes(temp_in, RSALENK/8);
    thread t1(&private_encrypt, temp_in, RSALENK/8, C->ID, temp_out);
    thread t2(&private_encrypt, temp_out, RSALENK/8, PL->ID, temp_out);
    t1.join();
    t2.join();

    // Send shares to the consumer
    thread t1n(&receiveValue, Cba, CbaPL, 16);
    thread t2n(&receiveValue, Cbx, CbxPL, 16);

    t1n.join();
    t2n.join();

    // Reconstruct the value in the shares
    unsigned char CbPL[16];
    for (int i = 0; i < 16; i++) {
        CbPL[i] = Cba[i] ^ Cbx[i];
    }

    unsigned char Mb[LCB];
    for (int i = 0; i < ISIGMA; i++) {
        Mb[i] = M[i];
    }
    for (int i = 0; i < LCB-ISIGMA; i++) {
        Mb[i+ISIGMA] = 0;
    }

    // Initialise our iv
    unsigned char iv[16];
    for (int i = 0; i < 16; i++) {
        iv[i] = 0;
    }

    unsigned char Cb[LCB];

    // Sign the original booking details with K2 that the consumer chose.
    // This result will later be compared with the token in the public ledger.
    AES_CBC_encrypt(Cb, (unsigned char*)Mb, LCB, C->K2, iv);
    // Update the iv
    for (int i = 0; i < 16; i++) {
        iv[i] = Cb[LCB-16+i];
    }

    // Check if Cb is correct-----------------------------------------------------

    // We check our local signature with the token from the public ledger.
    bool Cbcheck = 0;
    for (int i = 0; i < 16; i++) {
        if (CbPL[i] != Cb[LCB-16+i]) {
            Cbcheck = 1;
            break;
        }
    }

    if (Cbcheck) {
        cout << endl;
        cout << "The consumer rejects the token." << endl;
    } else {
        cout << "The consumer accepts the token." << endl;
    }

    free(CbaPL);
    free(CbxPL);
    free(Cba);
    free(Cbx);

}

// We check the correctness of the Cuccar access token by running the algorithm without MPC, with all the need information opened, we then compare the results
void checkCuccar(unsigned char* M, string Kstring, unsigned char* K1, PublicLedger* PL, Database* db, long time_stamp) {
    unsigned long count = 0;

    // Ready all the needed information for the encryptions
    unsigned char longM[LBOOKING];
    for (int i = 0; i < LBOOKING; i++) {
        longM[i] = M[i];
    }
    for (int i = LBOOKING; i < LBOOKING; i++) {
        longM[i] = 0;
    }

    unsigned char Kcar[16];
    for (int j = 15; j >= 0; j--) {
        Kcar[j] = (unsigned char) strtol(Kstring.substr((unsigned int)j*2, 2).c_str(), NULL, 16);
    }

    unsigned char Ccar[LCCAR];
    for(int i = 0; i < LCCAR; i++) {
        Ccar[i] = 0;
    }

    // Do the first encryption on the booking details via the car key

    for (int i = 0; i < LBOOKING; i+=KEYSIZE/8) {
        AES_CTR(((unsigned char*)longM)+i, Kcar, count, Ccar+i); // add i's
        count++;
    }

    for (int i = 0; i < 4; i++) {
        Ccar[LBOOKING+i] = M[IIDCAR+i];
    }

    for (int i = 4; i < 16; i++) {
        Ccar[LBOOKING+i] = 0;
    }

    unsigned char Cuccar[LCCAR];

    // Do the second encryption via the key K1 provided by the consumer

    for (int i = 0; i < LCCAR; i+=KEYSIZE/8) {
        AES_CTR(Ccar+i, K1, count, Cuccar+i);
        count++;
    }

    // Get the shares of the access token out of the public ledger via the time stamp
    string query_PL = "SELECT Cuccar1a, Cuccar3x FROM "+PL->ledger+" WHERE Time='"+to_string(time_stamp)+"';";
    vector<vector<string> > resultPL = db->query(query_PL.c_str());
    vector<vector<string> >::iterator it = resultPL.begin();
    vector<string> row = *it;

    unsigned char Cuccara[LCCAR];
    unsigned char Cuccarx[LCCAR];

    for (unsigned int i = 0; i < LCCAR; i++) {
        Cuccara[i] = (unsigned char) strtol(row.at(0).substr(i*2, 2).c_str(), NULL, 16);
        Cuccarx[i] = (unsigned char) strtol(row.at(1).substr(i*2, 2).c_str(), NULL, 16);
    }

    unsigned char CuccarPL[LCCAR];
    for (int i = 0; i < LCCAR; i++) {
        CuccarPL[i] = Cuccara[i] ^ Cuccarx[i];
    }

    // Check if Cuccar is correct-------------------------------------------------

    bool Cuccarcheck = 0;
    for (int i = 0; i < LCCAR; i++) {
        if (CuccarPL[i] != Cuccar[i]) {
            Cuccarcheck = 1;
            break;
        }
    }

    if (Cuccarcheck) {
        cout << endl;
        cout << "Something went wrong in the generation of Cuccar!" << endl << endl;

        cout << "Actual Cuccar: " << endl;
        for (int i = 0; i < LCCAR; i++) {
            cout << (int) Cuccar[i] << " ";
        }
        cout << endl << endl;

        cout << "MPC Cuccar: " << endl;
        for (int i = 0; i < LCCAR; i++) {
            cout << (int) CuccarPL[i] << " ";
        }
        cout << endl << endl;

    } else {
        cout << endl;
        cout << "Cuccar token generated correctly!" << endl;
    }
}

// Clear the consumer
void clean_consumer(Consumer* C) {
    free(C->K1);
    free(C->K2);

    remove((C->ID+"_public").c_str());
    remove((C->ID+"_private").c_str());
}

// Clear the owner
void clean_owner(Owner* O) {
    remove((O->ID+"_public").c_str());
    remove((O->ID+"_private").c_str());
}

// At the end we clear everything
void clean_servers(Server* S1, Server* S2, Server* S3, Server* CM, Database* db) {

    db->close();

    delete db;

    delete S1->S;
    delete S2->S;
    delete S3->S;
    delete CM->S;

    remove("PePTCAP.sqlite");

    remove("S1_public");
    remove("S2_public");
    remove("S3_public");
    remove("PL_public");

    remove("S1_private");
    remove("S2_private");
    remove("S3_private");
    remove("PL_private");
}

// The main function, this generates all the databases, adds keys, runs the car sharing protocol and checks the result
void shareCar() {

    srand((unsigned int) time(NULL));
    socket_startup();

    // Initialise Consumer--------------------------------------------------------

    Consumer C;

    INI_consumer(&C, "2");

    // Initialise Owners-----------------------------------------------------------

    Owner O1, O2, O3;

    INI_owner(&O1, "1");
    INI_owner(&O2, "3");
    INI_owner(&O3, "5");

    // Initialise Servers and PublicLedger-----------------------------------------

    Server CM, S1, S2, S3;
    PublicLedger PL;
    Database *db;

    string dbName = "PePTCAP.sqlite";
    db = new Database(dbName.c_str());

    INI_servers(&CM, &S1, &S2, &S3, &PL, db);

    // Make and add keys----------------------------------------------------------

    string key1 = "000102030405060708090a0b0c0d0e0f";
    string key2 = "00000000000000000000000000000000";
    string key3 = "00000000000000000000000000000001";
    string key4 = "00000000000000000000000000000002";
    string key5 = "00000000000000000000000000000003";
    string key6 = "00000000000000000000000000000004";


    addKey(&CM, &S1, &S2, &S3, db, O1.ID, "1", key1);
    addKey(&CM, &S1, &S2, &S3, db, O2.ID, "1", key2);
    addKey(&CM, &S1, &S2, &S3, db, O3.ID, "1", key3);
    addKey(&CM, &S1, &S2, &S3, db, O1.ID, "2", key4);
    addKey(&CM, &S1, &S2, &S3, db, O1.ID, "3", key5);
    addKey(&CM, &S1, &S2, &S3, db, O1.ID, "4", key6);

    // This is where we begin timing our protocol---------------------------------

    auto beginproto = chrono::high_resolution_clock::now();

    // Make booking details-------------------------------------------------------
    // These consist of the following:
    // Bytes   0 - 63  consist of the consumer's certificate
    // Bytes  64 - 67  consist of the ID of the car
    // Bytes  68 - 75  consist of the location of the car
    // Bytes  76 - 87  consist of a set of conditions on the use of the car
    // Byte   88       consists of the access rights for the consumer
    // Bytes  89 - 153 consist of the sha-256 hash of the owner's signature
    // Bytes 154 - 160 consists of zero padding

    unsigned char M[LBOOKING+LPAD];
    bool check = readBookingDetails(M, "booking_details");
    if (!check) {
        return;
    }

    // Send K1, K2----------------------------------------------------------------

    unsigned long K1_index;
    unsigned long K2_index;
    sendK1K2(&C, &O1, &S1, &S2, &S3, &K1_index, &K2_index);

    // Share booking details with the servers-------------------------------------

    int booking_id;
    unsigned long booking_server_id;
    shareM(&O1, &S1, &S2, &S3, db, M, &booking_id, &booking_server_id);

    // Pick the correct car key---------------------------------------------------

    unsigned long key_index;
    extractKey(O1.ID, &S1, db, booking_id, &key_index);

    // Make the access token------------------------------------------------------

    unsigned long Cuccar_index[LCCAR];
    unsigned long Cb_index[CBSIZE/8];
    makeToken(&S1, booking_server_id, key_index, K1_index, K2_index, Cuccar_index, Cb_index);

    // Send the value to the public ledger----------------------------------------

    long time_stamp;
    sendToPL(&S1, &S2, &S3, &PL, db, Cuccar_index, Cb_index, &time_stamp);

    // The consumer checks if the second access token is valid--------------------

    verifyToken(&C, &PL, db, M, time_stamp);

    // This is where our timing of our protocol stops-----------------------------

    auto endproto = chrono::high_resolution_clock::now();
    auto durationproto = chrono::duration_cast<chrono::milliseconds>(endproto-beginproto).count();

    cout << endl << "The protocol took: " <<  float(durationproto) / 1000.0 << " seconds" << endl;

    // Check if the calculation was done correctly--------------------------------

    checkCuccar(M, key4, C.K1, &PL, db, time_stamp);

    // Clean up-------------------------------------------------------------------

    clean_consumer(&C);

    clean_owner(&O1);
    clean_owner(&O2);
    clean_owner(&O3);

    clean_servers(&S1, &S2, &S3, &CM, db);

    socket_cleanup();
}
