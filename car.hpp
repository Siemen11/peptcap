#ifndef _CAR_HPP_
#define _CAR_HPP_

#include <stdio.h>
#include <string.h>
#include <sqlite3.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "database.hpp"
#include "party.hpp"
#include "RSA.hpp"
#include "protocol_tools.hpp"
#include "utils.hpp"
#include "operationFactory.hpp"

struct Server {
    Party* S;
    string keyTable;
    string bookingTable;
    string ID;
};

struct Owner {
    string ID;
};

struct Consumer {
    string ID;
    unsigned char* Kc;
    unsigned int counter;
    unsigned char* K1;
    unsigned char* K2;
};

struct PublicLedger {
    string ledger;
    string ID;
};

void INI_consumer(Consumer* C, string ID);

void INI_owner(Owner* O, string ID);

void INI_servers(Server* CM, Server* S1, Server* S2, Server* S3, PublicLedger* PL, Database* db);

void addKey(Server* CM, Server* S1, Server* S2, Server* S3, Database* db, string IDo, string IDcar, string key);

bool readBookingDetails(unsigned char* M, const char* booking_file);

void sendK1K2(Consumer* C, Owner* O, Server* S1, Server* S2, Server* S3, unsigned long* K1_index, unsigned long* K2_index);

void shareM(Owner* owner, Server* S1, Server* S2, Server* S3, Database* db, unsigned char* M, int* booking_server_id, unsigned long* booking_index);

void extractKey(string identity_owner, Server* S1, Database* db, int booking_id, unsigned long* key_index);

void makeToken(Server* S1, unsigned long booking_index, unsigned long key_index, unsigned long K1_index, unsigned long K2_index, unsigned long* Cuccar_index, unsigned long* Cb_index);

void sendToPL(Server* S1, Server* S2, Server* S3, PublicLedger* PL, Database* db, unsigned long* Cuccar_index, unsigned long* Cb_index, long* time_stamp);

void verifyToken(Consumer* C, PublicLedger* PL, Database* db, unsigned char* M, long time_stamp);

void checkCuccar(unsigned char* M, string Kstring, unsigned char* K1, PublicLedger* PL, Database* db, long time_stamp);

void clean_consumer(Consumer* C);

void clean_owner(Owner* O);

void clean_servers(Server* S1, Server* S2, Server* S3, Server* CM, Database* db);

void shareCar();

#endif
