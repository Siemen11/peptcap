# README #

* Quick summary: 
We design a concrete decentralised and Privacy-enhanced Protocol for (Temporary) Car Access Provision namely \protocol. A novel physical keyless car sharing protocol that allows users to share their cars conveniently. In spite of many advantages, keyless car sharing systems come with substantial security and privacy challenges. Within this work, \protocol\ uses multiparty computation based on threshold secret sharing scheme which allows the generation, update, revocation and distribution of an access token for a car in a privacy-preserving manner. In order to solve disputes and to deal with law enforcement requests, our protocol provides forensic evidence of car incidents based on threshold secret sharing. We perform a security and privacy analysis of \protocol\ followed by a complexity analysis, practical efficiency and time estimations for the multiparty computation elements of \protocol\ under realistic scenarios.

* Run: 
The executable is a standalone static executable, making it possible to run it immediately on a Linux operating system.
Note that you also need the executable of the server as well as it is used to simulate the communication.

* Contact: 
siemen.dhooghe@student.kuleuven.be