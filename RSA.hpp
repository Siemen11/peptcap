#ifndef _RSA_HPP_
#define _RSA_HPP_

#include <stdio.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>

using namespace std;

bool generate_key(string name, int key_length);

RSA * createRSA(unsigned char * key,int publicKey);

RSA * createRSAWithFilename(string filename, int publickey);

int public_encrypt(unsigned char * data,int data_len, string filename, unsigned char *encrypted);

int private_decrypt(unsigned char * enc_data,int data_len, string filename, unsigned char *decrypted);

int private_encrypt(unsigned char * data,int data_len, string filename, unsigned char *encrypted);

int public_decrypt(unsigned char * enc_data,int data_len, string filename, unsigned char *decrypted);

void printLastError(char *msg);

#endif
