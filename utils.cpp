#include "utils.hpp"
#include <openssl/aes.h>
#include <netinet/in.h>
#include <mutex>
#include<arpa/inet.h>

#define KEYLENGTH 128
#define PORTNO 3010
#define NSOCKETS 80
#define MAXLENGTH 12

using namespace std;

mutex list_mtx;

static vector<int> socket_pool;

void socket_startup() {
    if (USE_DELAY) {
        if (system("gnome-terminal -e 'sh -c \"./Server/server\"'") < 0) {
            printf("Server creation failed n");
        }
        struct sockaddr_in sock_var;
        sock_var.sin_addr.s_addr = inet_addr("127.0.0.1");
        sock_var.sin_port = PORTNO;
        sock_var.sin_family = AF_INET;

        for (int i = 0; i < NSOCKETS; i++) {
            socket_pool.push_back(socket(AF_INET, SOCK_STREAM, 0));

            if (connect(socket_pool[i], (struct sockaddr *) &sock_var, sizeof(sock_var)) < 0) {
                printf("socket creation failed \n");
            }
        }
    }
}

void socket_cleanup() {
    if (USE_DELAY) {
        for (int i = 0; i < NSOCKETS; i++) {
            close(socket_pool[i]);
        }
    }
}

void socket_simulation() {
    char str_clnt[MAXLENGTH+1],str_ser[MAXLENGTH+1];
    int socketfd;
    ssize_t n;

    for (int i = 0; i < MAXLENGTH; i++) {
        str_clnt[i] = (char)rand();
    }

    list_mtx.lock();
    socketfd = socket_pool[0];
    socket_pool.erase(socket_pool.begin());
    list_mtx.unlock();

    n = write(socketfd, str_clnt, (size_t)MAXLENGTH);
    if (n < 0) {
        cout << "Could not write to the socket" << endl;
    }
    n = read(socketfd, str_ser, (size_t)MAXLENGTH);
    if (n < 0) {
        cout << "Could not read from the socket" << endl;
    }

    list_mtx.lock();
    socket_pool.push_back(socketfd);
    list_mtx.unlock();

    // A check to see if the result is correct
    bool check = true;
    for (int i = 0; i < MAXLENGTH; i++) {
        if(str_clnt[i] != str_ser[i]) {
            check = false;
        }
    }
    if (!check) {
        cout << "The server dit not echo the correct message" << endl;
    }

}

// Simulate communication of one machine sending a packet to another machine
// We also simulate a TLS connection by adding AES encryptions (we assume keys were exchanged previously)
void simulate_communication(const int delay) {

    if (USE_DELAY) {

        const size_t encslength = ((MAXLENGTH*8 + AES_BLOCK_SIZE) / AES_BLOCK_SIZE) * AES_BLOCK_SIZE;
        unsigned char enc_out[encslength];
        unsigned char dec_out[MAXLENGTH*8];
        memset(enc_out, 0, sizeof(enc_out));
        memset(dec_out, 0, sizeof(dec_out));

        unsigned char iv_enc[AES_BLOCK_SIZE], iv_dec[AES_BLOCK_SIZE];
        memset(iv_enc, 0, sizeof(iv_enc));
        memset(iv_dec, 0, sizeof(iv_dec));

        unsigned char aes_key[KEYLENGTH/8];
        memset(aes_key, 0, KEYLENGTH/8);

        unsigned char aes_input[MAXLENGTH*8];
        memset(aes_input, 0, MAXLENGTH*8);

        AES_KEY enc_key, dec_key;

        AES_set_encrypt_key(aes_key, KEYLENGTH, &enc_key);
        AES_cbc_encrypt(aes_input, enc_out, MAXLENGTH*8, &enc_key, iv_enc, AES_ENCRYPT);

        bool package_loss = true;

        while(package_loss) {

            socket_simulation();

            struct timespec req = {0};
            req.tv_sec = 0;
            req.tv_nsec = delay;
            nanosleep(&req, (struct timespec *) NULL);

            if (((double) rand() / (RAND_MAX)) < 0.99) { // 1% chance of package loss
                package_loss = false;
            }
        }

        AES_set_decrypt_key(aes_key, KEYLENGTH, &dec_key);
        AES_cbc_encrypt(enc_out, dec_out, encslength, &dec_key, iv_dec, AES_DECRYPT);
    }
}

// Multiplication in GF(2^8)
uint8_t FFMul(uint8_t a, uint8_t b) {
    uint8_t aa = a, bb = b, r = 0, t;
    while (aa != 0) {
        if ((aa & 1) != 0)
            r = r ^ bb;
        t = (unsigned char)(bb & 0x80);
        bb = bb << 1;
        if (t != 0)
            bb = (unsigned char)(bb ^ 0x1b);
        aa = aa >> 1;
    }
    return r;
}

// Exponentiation by 254
uint8_t exp254(uint8_t base) {
    uint8_t two = FFMul(base, base);

    uint8_t four = FFMul(two, two);
    uint8_t eight = FFMul(four, four);
    uint8_t nine = FFMul(eight, base);
    uint8_t eighteen = FFMul(nine, nine);
    uint8_t nineteen = FFMul(eighteen, base);
    uint8_t thirtysix = FFMul(eighteen, eighteen);
    uint8_t fiftyfive = FFMul(thirtysix, nineteen);
    uint8_t seventytwo = FFMul(thirtysix, thirtysix);
    uint8_t hundredtwentyseven = FFMul(seventytwo, fiftyfive);
    uint8_t twohundredfiftyfour = FFMul(hundredtwentyseven, hundredtwentyseven);

    return twohundredfiftyfour;
}

// Sha256 hash function
void sha256(char *string, char outputBuffer[64]) {
    unsigned char hash[SHA256_DIGEST_LENGTH];

    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, string, strlen(string));
    SHA256_Final(hash, &sha256);

    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
    }
    outputBuffer[64] = 0;
}